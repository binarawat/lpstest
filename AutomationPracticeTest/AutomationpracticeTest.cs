﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using NUnit;

namespace AutomationPracticeTest
{
    [TestClass]
    public class AutomationpracticeTest
    {
        IWebDriver Driver;
        private static string Email, Password;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Email = ConfigurationManager.AppSettings["Email"];
            Password = ConfigurationManager.AppSettings["Password"];
        }

        [TestInitialize]
        public void OpenBrowser()
        {
            Driver = new ChromeDriver();
        }

        [TestMethod]
        public void Should_Add_T_Shirt_To_Cart()
        {  
            Driver.Url = "http://automationpractice.com";

            try
            {
                //LOGIN
                Driver.FindElement(By.ClassName("login")).Click();
                Driver.FindElement(By.Id("email")).SendKeys(Email);  //XPATH=input[@id='email']
                Driver.FindElement(By.Id("passwd")).SendKeys(Password);
                Driver.FindElement(By.Id("SubmitLogin")).Click();
                
                //Open Womens Category
                Driver.FindElement(By.XPath("//div[@id='block_top_menu']/ul/li/a[@title='Women']")).Click();

                //Click on + (open sub category)
                Driver.FindElement(By.XPath("//div[@id='categories_block_left']/div/ul/li/span")).Click();

                //Find "t shirts"
                Driver.FindElement(By.XPath("//div[@class='block_content']//ul//li/ul/li[//text()[normalize-space() = 'T-shirts']]")).Click();

                //Find all t shirts
                var products = Driver.FindElements(By.XPath("//ul[@class='product_list grid row']//li/div[@class='product-container']//a[@class='product_img_link']")); //find all t shirts
                                                                                                                                                                        //If t shirts exist open first t shirt
                if (products.Count > 0)
                    products[0].Click();

                //Read product details
                var name = Driver.FindElement(By.XPath("//div[@class='pb-center-column col-xs-12 col-sm-4']//h1[@itemprop='name']")).Text;
                Assert.IsNotNull(name);
                var productReference = Driver.FindElement(By.XPath("//div[@class='pb-center-column col-xs-12 col-sm-4']/p[@id='product_reference']")).Text;
                Assert.IsNotNull(productReference);
                var productCondition = Driver.FindElement(By.XPath("//div[@class='pb-center-column col-xs-12 col-sm-4']/p[@id='product_condition']")).Text;
                Assert.IsNotNull(productCondition);
                var description = Driver.FindElement(By.XPath("//div[@id='short_description_content']/p")).Text;
                Assert.IsNotNull(description);
                var price = Driver.FindElement(By.XPath("//span[@id='our_price_display']")).Text;
                Assert.IsNotNull(price);
                MessageBox.Show(string.Join("\n", name, productReference, productCondition, description, "Price: " + price), "Test Output", MessageBoxButtons.OK);

                //Add to cart
                Driver.FindElement(By.XPath("//p[@id='add_to_cart']/button[@type='submit']")).Click();

                //Should be added
                var iconAdded = Driver.FindElements(By.XPath("//*[@id='layer_cart']//h2/i[@class='icon-ok']")).Count;
                //count should be > 0
                Assert.IsTrue(iconAdded > 0);
            }
            catch (NoSuchElementException exception)
            {
                MessageBox.Show(string.Format("Element not found {0}{1}0}{2}", Environment.NewLine, exception.Message, exception.StackTrace));
            }
           
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            Driver.Close();
        }
    }

}
